![](./New_LOGO_CryptoHelper.png)

CryptoHelper by @FallFur
- Cifrado híbrido:
 > RSA 4096-bits asimétrico
 > Cifrado simétrico TRIPLE capa [Cipher Block Chaining – CBC]
 > Aderezo personal [salt]
- Compresión y empaquetado en formato custom (.crypth)
- Esteganografía EoF stealth
- Esteganografía LSB custom - privado
- Upload anónimo a internet
- Generador de claves pública/privada RSA seguras
- Instalador de la última versión dev de OpenSSL

