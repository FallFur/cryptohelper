#!/bin/bash

# Este programa genera una key simétrica para cifrar ficheros grandes
# La llave simétrica será cifrada con la llave pública que se indique
# by @FallFur

# DEFINICIÓN DE VARIABLES

# SE RECOMIENDA ENCARECIDAMENTE MODIFICAR EL VALOR DE LA VARIABLE $customsalt DE A CONTINUACIÓN POR OTRO VALOR
# SALT
customsalt=9A876CF54B32DD1A # MODIFICAR

# Colores
bold=$(tput bold)		# Bold
red=$bold$(tput setaf 1)	# Red
green=${bold}$(tput setaf 2) 	# Green
yellow=${bold}$(tput setaf 3) 	# Yellow
blue=${bold}$(tput setaf 4) 	# Blue
purple=${bold}$(tput setaf 5) 	# Purple
cyan=${bold}$(tput setaf 6) 	# Cyan
gray=$(tput setaf 7)            # dim white text
white=${bold}$(tput setaf 7) 	# White
rst=$(tput sgr0) 		# Text reset

# Help
case "$1" in
	-h|--help)
		echo "${white}DESCRIPCIÓN:${rst}"
		echo "Este programa es un wizard que te ayuda a generar de manera segura un fichero cifrado"
		echo "Generará un fichero zip con el fichero cifrado y la clave simétrica cifrada con la clave pública del receptor"
		echo "";
		echo "¡Mete los ficheros que quieras cifrar y la clave pública en la misma carpeta que este script!"
		echo ""
		echo "Requerimientos: clave pública del receptor" 
		echo ""
		echo "${white}PARAMETROS:${rst}"
		echo "${green}-h|--help -->${rst}"
		echo " Muestra este menu"
		echo "${green}-i|--update-openssl -->${rst}"
		echo " Instala la ultima version de OpenSSL necesario para evitar problemas"
		echo " esto es de compatibilidad con otras versiones de OpenSSL ${red}"
		echo " (¡CUIDADO! Se sustituirá tu versión de OpenSSL a la ultima)${rst}"
		echo ""

		exit 0;;
	-i|--update-openssl)
		echo "Este proceso sustituirá tu versión de OpenSSL a la ultima y va a tardar lo suyo..."
		cd /tmp
		sudo git clone https://github.com/openssl/openssl.git
		sudo mv openssl /usr/src
		cd /usr/src/openssl
		sudo ./config
		sudo make
		sudo make install
		sudo mv /usr/bin/openssl /root/
		sudo ln -s /usr/local/ssl/bin/openssl /usr/bin/openssl
		sudo ldconfig /usr/local/lib64/
		echo ""
		echo "Versión de OpenSSL instalada:"
		openssl version
		exit 0;;
	-*) echo "Bad argument $1"; echo ""; echo "Use: ";
		echo "-h|--help for help"
		echo "-i|--update-openssl"
		exit 1;;
	#*) echo "Bad argument $1"; exit 1
esac

# Evitar problemas
set pipefail
set -e


# DEFINICIÓN DE FUNCIONES

# KILL signals processing
ksignal() {
        # Limpieza de ficheros generados...
		echo "${red}"
		echo "    .__   .-\"."
		echo "   (o\\\"\  |  |"
		echo "      \_\ |  | Cuantas prisas no?"
		echo "     _.---:_ |"
		echo "    (\"-..-\" /"
		echo "     \"-.-\" /"
		echo "       /   | limpiando temporales..."
		echo "       \"--\"   ${rst}"
        rm -r .temp_crypth/ 2> /dev/null
        exit 1
}

# banner principal: título + autor
banner() {
	clear
	echo "${green}                              __           .__           .__                       ";
	echo "   ___________ ___.__._______/  |_  ____   |  |__   ____ |  | ______   ___________ ";
	echo " _/ ___\\_  __ <   |  |\\____ \\   __\\/  _ \\  |  |  \\_/ __ \\|  | \\____ \\_/ __ \\_  __ \\";
	echo " \  \___|  | \/\___  ||  |_> >  | (  <_> ) |   Y  \  ___/|  |_|  |_> >  ___/|  | \/";
	echo "  \___  >__|   / ____||   __/|__|  \____/  |___|  /\___  >____/   __/ \___  >__|   ";
	echo "      \/       \/     |__|                      \/     \/     |__|        \/       ${rst}${red}v0.9.1${str}";
	echo "";
	echo "${purple} Con mucho cariño by @ƒαll ~⠠⠵${rst}";
	echo " ";
}

# menu principal
menu() {
		banner
		echo "    1. Cifrar fichero"
		echo "    2. Descifrar fichero"
		echo "    3. Enviar fichero"
		echo "    4. Utilidades"
		echo "    0. ¡Sácame de aquí!"
		echo ""
}

leer_opciones() {
	local choice
	read -p " ${white}Seleccione una opción [ 1 - 5 ] ${rst}" choice
	case $choice in
		1) menu_cifrado ;;
		2) menu_descifrado ;;
		3) menu_envio ;;
		4) menu_utilidades ;;
		0) limpieza; exit 0;;
		*) echo " ${red}Escriba una opción válida${rst}" && sleep 2

	esac
}

menu_utilidades() {
	banner
        echo "    1. Generar claves Pública/Privada"
		echo "    2. Comprobar salud de claves"
		echo "    3. Actualizar/Instalar OpenSSL"
        echo "    0. Volver al menú"
        echo ""

        local choice
        read -p " ${white}Seleccione una opción [ 1 - 3 ] ${rst}" choice
        case $choice in
                1) gen_claves ;;
                2) menu_estado_claves ;;
				3) actualizar_openssl ;;
                0) menu leer_opciones ;;
                *) echo "${red}Escriba una opción válida${rst}" && sleep 2 ; menu_utilidades

        esac
}

menu_cifrado() {
		banner
		openssl version
		echo ""
        echo "    1. Cifrar formato ${green}Crypth${rst} ${yellow}[Triple capa de cifrado]${rst}"
        echo "    2. Cifrar formato ${green}Crypth + Esteganografía${rst} ${yellow}[Tripe capa de cifrado]${rst}"
        echo "    0. Volver al menú"
        echo ""

        local choice
        read -p " ${white}Seleccione una opción [ 1 - 3 ] ${rst}" choice
        case $choice in
                1) cifrar_fichero ;;
                2) menu_stego ;;
                0) menu leer_opciones ;;
                *) echo "${red}Escriba una opción válida${rst}" && sleep 2 ; menu_cifrado

        esac
}

menu_stego() {
		banner
		openssl version
		echo ""
        echo "    1. Cifrado + esteganografía ${green}[ EOF ]${rst} ${yellow}[Tapadera: cualquier formato]${rst} ${gray}Para datos pesados, incluso más que el estegomedio${rst}"
        echo "    2. Cifrado + esteganografía ${green}[ LSB ]${rst} ${yellow}[Tapadera: Imágen PNG]${rst} ${gray}Jodidamente difícil de estegoanalizar, tamaño a ocultar limitado al peso del estegomedio${rst}"
        echo "    0. Volver al menú"
        echo ""

        local choice
        read -p " ${white}Seleccione una opción [ 1 - 3 ] ${rst}" choice
        case $choice in
                1) cifrar_stego_eof ;;
                2) cifrar_stego_lsb ;;
                0) menu leer_opciones ;;
                *) echo "${red}Escriba una opción válida${rst}" && sleep 2 ; menu_stego

        esac
}

menu_descifrado() {
		banner
		openssl version
		echo ""
        echo "    1. Descifrar formato ${green}Crypth${rst}"
        echo "    2. Descifrar formato ${green}Crypth + Esteganografía EoF${rst}"
        echo "    3. Descifrar formato ${green}Crypth + Esteganografía LSB${rst}"
        echo "    0. Volver al menú"
        echo ""

        local choice
        read -p " ${white}Seleccione una opción [ 1 - 3 ] ${rst}" choice
        case $choice in
                1) descifrar_fichero ;;
                2) descifrar_fichero_stego_eof ;;
				3) descifrar_fichero_stego_lsb ;;
                0) menu leer_opciones ;;
                *) echo "${red}Escriba una opción válida${rst}" && sleep 2 ; menu_descifrado

        esac
}

menu_envio() {
	banner
        echo "    1. ${white}Anonfiles ${yellow}[20GB limit]${rst}"
		echo "    2. ${white}Anonfiles${rst} ${gray}-->${rst} ${green}TOR${rst} ${gray}(envío más anónimo)${rst} ${yellow}[20GB limit]${rst}"
        echo "    0. Volver al menú"
        echo ""

        local choice
        read -p " ${white}Seleccione una opción [ 1 - 3 ] ${rst}" choice
        case $choice in
                1) upload_anonfiles ;;
                2) upload_anonfiles_tor ;;
                0) menu leer_opciones ;;
                *) echo "${red}Escriba una opción válida${rst}" && sleep 2 ; menu_envio

        esac
}

listado() {
	echo "${green}______________________________________________________________________________${rst}";
#	echo "~$ pwd"
#	pwd
#	echo "";
#	echo "~$ ls"
#	ls
#	echo "${green}______________________________________________________________________________${rst}";
	echo "";
}

limpieza() {
        # Limpieza de ficheros generados...
        echo ""
        echo "limpiando..."
        rm -r .temp_crypth/ 2> /dev/null
        echo "";
}

cifrar_fichero() {
	# Crear directorio temporal
	if [ -d .temp_crypth/ ];
	then
		rm -r .temp_crypth/
		mkdir .temp_crypth/
	else
		mkdir .temp_crypth/
	fi

	# Generar clave simétrica
	echo "Generando llave secreta..."
	dd if=/dev/urandom of=.temp_crypth/.secretkey1 bs=20 count=10 &>/dev/null
	dd if=/dev/urandom of=.temp_crypth/.secretkey2 bs=20 count=10 &>/dev/null
	dd if=/dev/urandom of=.temp_crypth/.secretkey3 bs=20 count=10 &>/dev/null
	echo ""; sleep 1

	listado

	# El usuario define fichero a cifrar con la llave generada con método sm4
	echo -n "${purple} #${rst}"
	read -e -p ' Ruta del fichero a cifrar SIN ESPACIOS: ' file_to_crypt
	
	if [  -f "${file_to_crypt}" ]; then
	echo "Cifrando fichero ${green}${file_to_crypt}${rst} "; sleep 1

	# Empaquetado del fichero a cifrar
	zip -q -j .temp_crypth/crypth_DATA.zip "${file_to_crypt}"

	# Cifrado de fichero
	openssl enc -aes-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/.secretkey1 < .temp_crypth/crypth_DATA.zip > .temp_crypth/crypt_data.1
	openssl enc -camellia-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/.secretkey2 < .temp_crypth/crypt_data.1 > .temp_crypth/crypt_data.2
	openssl enc -aria-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/.secretkey3 < .temp_crypth/crypt_data.2 > .temp_crypth/x

	#banner

	echo "Su fichero ${green}${file_to_crypt}${rst} se ha cifrado"
	
	listado
	
	# Cifrar llave con clave pública de receptor
	echo "Ahora, indique la calve pública del receptor"
	echo "Ej: ${blue}/home/hax0r/Documents/RSA_KEY/hax0r_rsa_public.pem${rst}"
	echo -n "${purple} #${rst}"
	read -e -p ' Ruta a la Clave Pública del receptor SIN ESPACIOS: ' pubkey_to_crypt
	
	if [ -f "${pubkey_to_crypt}" ]; then

	openssl rsautl -encrypt -inkey ${pubkey_to_crypt} -pubin -in .temp_crypth/.secretkey1 -out .temp_crypth/1
	openssl rsautl -encrypt -inkey ${pubkey_to_crypt} -pubin -in .temp_crypth/.secretkey2 -out .temp_crypth/2
	openssl rsautl -encrypt -inkey ${pubkey_to_crypt} -pubin -in .temp_crypth/.secretkey3 -out .temp_crypth/3
	
	echo ""
	echo "Llaves simétricas generadas correctamente"; sleep 1
	sleep 3
	echo "";

	# Fase para machacar la clave simétrica anteriormente
	echo " ${red}### PROCEDIENDO A PURGA LAS LLAVES SECRETAS ###"
	shred --size=10M --iterations=10 -u --verbose --zero --remove .temp_crypth/.secretkey?
	echo "${rst}";

	# Compresión de ficheros en fichero ZIP
	echo "Empaquetando su fichero y llaves..."
	echo "";
	echo -n "${purple} #${rst}"
	read -e -p " Nombre del empaquetado final SIN ESPACIOS: " nombre_empaquetado
	
	zip -q --junk-paths .temp_crypth/zip2format.zip .temp_crypth/x .temp_crypth/1 .temp_crypth/2 .temp_crypth/3
	sleep 2

	# Eliminación de los Magic Numbers para dificultar su interpretación
	# PARA MAYOR SEGURIDAD, SE RECOMIENDAN PERSONALIZAR LOS VALORES HEXADECIMALES
	# QUE SUSTITUIRÁN A LOS MAGIC NUMBERS
	# MODIFICAR				 <------------->
	sed 's/\x50\x4b\x03\x04/\x1a\x1b\x1c\x1d/g' .temp_crypth/zip2format.zip > .temp_crypth/1
	# MODIFICAR		         <------------->
	sed 's/\x50\x4b\x01\x02/\x2a\x2b\x2c\x2d/g' .temp_crypth/1 > .temp_crypth/2
	# MODIFICAR   			 <------------->
	sed 's/\x50\x4b\x05\x06/\x3a\x3b\x3c\x3d/g' .temp_crypth/2 > ${nombre_empaquetado}.crypth

	limpieza
	
	# Despedida
	echo " ${yellow}Su fichero se ha cifrado correctamente, puede localizarlo en:${rst} ${green}$PWD/${nombre_empaquetado}.crypth${rst}"
	echo ""
	echo "Para descifrar el contenido, el receptor debe usar la función:"
	echo " ${green}\"Descifra Fichero --> Descifrar formato Crypth\"${rst} del menu principal ;)";
	echo "";
	echo " ${blue}https://hackmadrid.org/${rst}";
	echo "Happy Hacking - @FallFur ~⠠⠵";
	echo "${white}[ENTER]${rst}"; read acept
	menu
else
	echo "${red}La clave pública $pubkey_to_crypt no existe${rst}";
	cifrar_fichero
fi
else
	echo "${red}El fichero $file_to_crypt no existe${rst}";
	cifrar_fichero
fi
}

cifrar_stego_eof() {

	# Crear directorio temporal
	if [ -d .temp_crypth/ ];
	then
		rm -r .temp_crypth/
		mkdir .temp_crypth/
	else
		mkdir .temp_crypth/
	fi

	# Generación de claves simétricas aleatorias con valores chungos de pelotas
	echo ""
	echo "Generando llave secreta..."
	dd if=/dev/urandom of=.temp_crypth/.secretkey1 bs=20 count=10 &>/dev/null
	dd if=/dev/urandom of=.temp_crypth/.secretkey2 bs=20 count=10 &>/dev/null
	dd if=/dev/urandom of=.temp_crypth/.secretkey3 bs=20 count=10 &>/dev/null
	echo ""; sleep 1
    
	listado

	# El usuario define fichero a cifrar con la llave generada con método sm4
	echo -n "${purple} #${rst}"
	read -e -p ' Ruta del fichero a cifrar SIN ESPACIOS: ' file_to_crypt
	if [  -f "${file_to_crypt}" ]; then
	echo "Cifrando fichero ${green}${file_to_crypt}${rst} "; sleep 1

	# Empaquetado del fichero a cifrar
	zip -q -j .temp_crypth/crypth_DATA.zip "${file_to_crypt}"

	# Cifrado de fichero
	openssl enc -aes-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/.secretkey1 < .temp_crypth/crypth_DATA.zip > .temp_crypth/crypt_data.1
	openssl enc -camellia-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/.secretkey2 < .temp_crypth/crypt_data.1 > .temp_crypth/crypt_data.2
	openssl enc -aria-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/.secretkey3 < .temp_crypth/crypt_data.2 > .temp_crypth/x

	#banner
	echo "Su fichero ${green}${file_to_crypt}${rst} se ha cifrado"

	# Cifrar llave con clave pública de receptor
	
	listado
	
	echo "Ej: ${blue}/home/hax0r/Documents/RSA_KEY/hax0r_rsa_public.pem${rst}"
	echo -n "${purple} #${rst}"
	read -e -p ' Ruta a la clave Pública del receptor SIN ESPACIOS: ' pubkey_to_crypt
	
	if [  -f "${pubkey_to_crypt}" ]; then
	openssl rsautl -encrypt -inkey ${pubkey_to_crypt} -pubin -in .temp_crypth/.secretkey1 -out .temp_crypth/1
	openssl rsautl -encrypt -inkey ${pubkey_to_crypt} -pubin -in .temp_crypth/.secretkey2 -out .temp_crypth/2
	openssl rsautl -encrypt -inkey ${pubkey_to_crypt} -pubin -in .temp_crypth/.secretkey3 -out .temp_crypth/3
	sleep 1
	echo ""
	echo "Llaves simétricas cifradas generada correctamente"; sleep 1
	echo "";

	# Fase para machacar la clave simétrica anteriormente
	echo " ${red}### PROCEDIENDO A PURGA LAS LLAVES SECRETAS ###"
	shred --size=10M --iterations=10 -u --verbose --zero --remove .temp_crypth/.secretkey?
	echo "${rst}";

	# Compresión de ficheros en fichero ZIP
	zip -q --junk-paths .temp_crypth/zip2format.zip .temp_crypth/x .temp_crypth/1 .temp_crypth/2 .temp_crypth/3
	sleep 1
	echo ""
	
	# Eliminación de los Magic Numbers para dificultar su interpretación
	# PARA MAYOR SEGURIDAD, SE RECOMIENDAN PERSONALIZAR LOS VALORES HEXADECIMALES
	# QUE SUSTITUIRÁN A LOS MAGIC NUMBERS
	# MODIFICAR				 <------------->
	sed 's/\x50\x4b\x03\x04/\x1a\x1b\x1c\x1d/g' .temp_crypth/zip2format.zip > .temp_crypth/1
	# MODIFICAR		         <------------->
	sed 's/\x50\x4b\x01\x02/\x2a\x2b\x2c\x2d/g' .temp_crypth/1 > .temp_crypth/2
	# MODIFICAR   			 <------------->
	sed 's/\x50\x4b\x05\x06/\x3a\x3b\x3c\x3d/g' .temp_crypth/2 > .temp_crypth/z
	
	clear
	listado

	echo " ${yellow} Datos cifrados, empaquetados y procesados${rst} ${green}CORRECTAMENTE${rst}"
	echo ""

	# Selección de imagen para realizar la esteganografía
	echo -n "${purple} #${rst}"
	read -e -p ' Ruta SIN ESPACIOS del fichero tapadera para ocultar la información: ' stego_medio
	echo ""

	if [  -f "${stego_medio}" ]; then
	echo "${green}Esteganografiando medio...${rst}"
	echo ""
	sleep 2
	
	# Proceso de estegranografiado EoF (End of File)
	file_to_stego="${stego_medio##*/}"
	cat "${stego_medio}" .temp_crypth/z > STEGO_${file_to_stego}
	echo " ${red}### PROCEDIENDO A PURGA DEL FICHERO CIFRADO ###"
	shred --size=10M --iterations=10 -u --verbose --zero --remove .temp_crypth/x
	echo "${rst}"

	limpieza
	
	# Despedida
	echo " ${yellow}TODO CORRECTO, puede localizar su fichero STEGO_EOF en:${rst} ${green}$PWD${rst}"
	echo ""
	echo "Para extraer y descifrar el contenido, el receptor debe usar la función:"
	echo " ${green}\"Descifrar fichero --> Descifrar formato Crypth + Esteganografía EoF\"${rst} del menu principal ;)";
	echo "";
	echo " ${blue}https://hackmadrid.org/${rst}";
	echo "Happy Hacking - @FallFur ~⠠⠵";
	echo "${white}[ENTER]${rst}"; read acept
	menu
else
	echo "${red}El fichero $stego_medio no existe${rst}";
	cifrar_stego_eof
fi
else
	echo "${red} La clave pública $pubkey_to_crypt no existe${rst}";
	cifrar_stego_eof
fi
else
	echo "${red}El fichero $file_to_crypt no existe${rst}";
	cifrar_stego_eof
fi
}

cifrar_stego_lsb(){
	echo ""
	echo ""
	echo "${red}                           ${green}${rst} ${white}FUNCIÓN PRIVADA${rst} ${green}${rst}"${rst}
	echo ""
	echo "${red}  ¿Quieres un algoritmo único y seguro para comunicaciones encubiertas?"${rst}
	echo "  Móntate tu propio algoritmo LSB personalizado, en un futuro publicaré algún algoritmo${rst}"
	echo ""
	echo "${white}[ENTER]${rst}"; read acept

	menu_stego
}

descifrar_fichero() {
	# Crear directorio temporal
        if [ -d .temp_crypth/ ];
        then
                rm -r .temp_crypth/
                mkdir .temp_crypth/
        else
                mkdir .temp_crypth/
        fi
	# Intro
	echo ""
	echo "Este programa procederá a descifrar un paquete ${green}".crypth"${rst}"
	echo ""
	echo -n "${purple} #${rst}"
	read -e -p ' Indique la ruta del paquete a descifrar SIN ESPACIOS: ' data_to_decrypt

	# Recomposición de los Magic Numbers para posibilitar su interpretación
	# PARA MAYOR SEGURIDAD, SE RECOMIENDAN PERSONALIZAR LOS VALORES HEXADECIMALES
	# QUE SUSTITUIRÁN A LOS MAGIC NUMBERS

	# MOD   <-------------->
	sed 's/\x1a\x1b\x1c\x1d/\x50\x4b\x03\x04/g' ${data_to_decrypt} > .temp_crypth/data2format1
	# MOD   <-------------->
	sed 's/\x2a\x2b\x2c\x2d/\x50\x4b\x01\x02/g' .temp_crypth/data2format1 > .temp_crypth/data2format2
	# MOD   <-------------->
	sed 's/\x3a\x3b\x3c\x3d/\x50\x4b\x05\x06/g' .temp_crypth/data2format2 > .temp_crypth/formateddata.zip

	# Desempaquetado
	unzip -q .temp_crypth/formateddata.zip -d .temp_crypth/
	
	echo ""
	echo "Ej: ${blue}/home/hax0r/Documents/RSA_KEY/hax0r_rsa_private.key${rst}"
	echo -n "${purple} #${rst}"
	read -e -p ' Indique la ruta SIN ESPACIOS de la clave Privada de quién iba destinado la información: ' privkey_to_decrypt
	echo ""
	echo "${red}INDIQUE LA CONTRASEÑA DE LA CLAVE PRIVADA${rst} "

	# Descifrado de claves simetricas
	openssl rsautl -decrypt -inkey ${privkey_to_decrypt} -in .temp_crypth/1 -out .temp_crypth/secretkey1.txt
	openssl rsautl -decrypt -inkey ${privkey_to_decrypt} -in .temp_crypth/2 -out .temp_crypth/secretkey2.txt
	openssl rsautl -decrypt -inkey ${privkey_to_decrypt} -in .temp_crypth/3 -out .temp_crypth/secretkey3.txt

	echo "${blue}Descifrando...${rst}"
	sleep 3

	# Descifrado de fichero principal
	openssl enc -d -aria-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/secretkey3.txt < .temp_crypth/x > .temp_crypth/crypt_data.1
	openssl enc -d -camellia-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/secretkey2.txt < .temp_crypth/crypt_data.1 > .temp_crypth/crypt_data.2
	openssl enc -d -aes-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/secretkey1.txt < .temp_crypth/crypt_data.2 > decrypted_data_for_${USER}.zip
	
	clear
	echo ""
	echo " ${red}### PROCEDIENDO A PURGA LAS LLAVES SECRETAS ###"
	
	shred --size=10M --iterations=5 -u --verbose --zero --remove .temp_crypth/secretkey?.txt

	limpieza
	echo "${rst}";

	echo "Fichero ${green}${file_resultado}${rst} descifrado con éxito! :)"
	echo "Acceda a los datos en el fichero ${green}$PWD/decrypted_data_for_${USER}.zip${rst}"
	echo "¡Hurra por la privacidad!"
	echo "";
	echo " ${blue}https://hackmadrid.org/${rst}";
	echo "Happy Hacking ⠠⠵";

	echo "${white}[ENTER]${rst}"; read acept
	
	menu
}

descifrar_fichero_stego_eof() {
# Crear directorio temporal
if [ -d .temp_crypth/ ]; then
	rm -r .temp_crypth/
	mkdir .temp_crypth/
else
	mkdir .temp_crypth/
fi
    
    banner
    
	# Intro
	echo ""
	echo "Este programa procederá a descifrar un fichero con ${green}esteganografía "EoF"${rst} generado con esta herramienta"
	echo ""
	echo -n "${purple} #${rst}"
	read -e -p ' Indique ruta SIN ESPACIOS al fichero que contiene la información oculta: ' file_with_stego
	
	# Recomposición de los Magic Numbers para posibilitar su interpretación
	# PARA MAYOR SEGURIDAD, SE RECOMIENDAN PERSONALIZAR LOS VALORES HEXADECIMALES
	# QUE SUSTITUIRÁN A LOS MAGIC NUMBERS
	# MOD   <-------------->
	sed 's/\x1a\x1b\x1c\x1d/\x50\x4b\x03\x04/g' ${file_with_stego} > .temp_crypth/data2format1
	# MOD   <-------------->
	sed 's/\x2a\x2b\x2c\x2d/\x50\x4b\x01\x02/g' .temp_crypth/data2format1 > .temp_crypth/data2format2
	# MOD   <-------------->
	sed 's/\x3a\x3b\x3c\x3d/\x50\x4b\x05\x06/g' .temp_crypth/data2format2 > .temp_crypth/formateddata.zip

	# Decompresión del fichero con stego EoF
	unzip -q .temp_crypth/formateddata.zip -d .temp_crypth/ || true
	
	listado

	echo "Ej: ${blue}/home/hax0r/Documents/RSA_KEY/hax0r_rsa_private.key${rst}"
	echo -n "${purple} #${rst}"
	read -e -p ' Indique SIN ESPACIOS la clave Privada de quién iba destinado el paquete: ' privkey_to_decrypt
	echo ""
	echo "${red}INDIQUE LA CONTRASEÑA DE LA CLAVE PRIVADA${rst} "
	
	# Descifrado de claves asimetricas
	openssl rsautl -decrypt -inkey ${privkey_to_decrypt} -in .temp_crypth/1 -out .temp_crypth/secretkey1.txt
	openssl rsautl -decrypt -inkey ${privkey_to_decrypt} -in .temp_crypth/2 -out .temp_crypth/secretkey2.txt
	openssl rsautl -decrypt -inkey ${privkey_to_decrypt} -in .temp_crypth/3 -out .temp_crypth/secretkey3.txt
	
	echo "${blue}Descifrando...${rst}"
	sleep 3
	
	# Descifrado de fichero cifrado principal
	openssl enc -d -aria-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/secretkey3.txt < .temp_crypth/x > .temp_crypth/crypt_data.1
	openssl enc -d -camellia-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/secretkey2.txt < .temp_crypth/crypt_data.1 > .temp_crypth/crypt_data.2
	openssl enc -d -aes-256-cbc -a -salt -S $customsalt -pass file:.temp_crypth/secretkey1.txt < .temp_crypth/crypt_data.2 > decrypted_data_for_${USER}.zip
	
	echo ""
	echo " ${red}### PROCEDIENDO A PURGA LAS LLAVES SECRETAS ###"
	shred --size=10M --iterations=5 -u --verbose --zero --remove .temp_crypth/secretkey?.txt

	limpieza

	echo "${rst}";


	echo "Fichero ${green}${file_with_stego}${rst} descifrado con éxito! :)"
	echo "Acceda a los datos en el fichero ${green}$PWD/decrypted_data_for_${USER}.zip${rst}"
	echo "¡Hurra por la privacidad!"
	echo "";
	echo " ${blue}https://hackmadrid.org/${rst}";
	echo "Happy Hacking ⠠⠵";

	echo "${white}[ENTER]${rst}"; read acept
	
menu
}

descifrar_fichero_stego_lsb() {
	echo ""
	echo ""
	echo "${red}                           ${green}${rst} ${white}FUNCIÓN PRIVADA${rst} ${green}${rst}"${rst}
	echo ""
	echo "${red}  ¿Quieres un algoritmo único y seguro para comunicaciones encubiertas?"${rst}
	echo "  Móntate tu propio algoritmo LSB personalizado, en un futuro publicaré algún algoritmo${rst}"
	echo ""
	echo "${white}[ENTER]${rst}"; read acept

	menu_stego
}

#OPCIONES DE ENVIO
upload_anonfiles() {

# Crear directorio temporal
if [ -d .temp_crypth/ ]; then
		rm -r .temp_crypth/
		mkdir .temp_crypth/
	else
		mkdir .temp_crypth/
fi

echo ""

#Comprobar binarios requeridos cURL
if ! which curl > /dev/null; then
		echo "${red}El script no ha encontrado el binario de cURL en el \$PATH"
		echo "Porfavor, instala cURL en el sistema${rst}"
		echo "${white}[ENTER]${rst}"; read acept
		menu
	else
		echo "Se va a subir el fichero que indiques a ${blue}www.anonfiles.com${rst}, su fichero podría ser accesible por otras personas"
		echo -n "${purple} #${rst}"
		read -e -p ' Indique el fichero a enviar SIN ESPACIOS: ' file_2_upload
		echo ""
		if [  -f ${file_2_upload} ]; then
		echo "Subiendo fichero..."
		curl -# -F "file=@$file_2_upload" https://api.anonfiles.com/upload > .temp_crypth/.jsondata_anonfiles.api
		url_anonfiles="$(cat .temp_crypth/.jsondata_anonfiles.api | awk -F'"' '/full/{gsub(/\\/, "", $4); print $4}')"
		echo ""
		echo "Link de descarga: ${blue}$url_anonfiles${rst}"
		echo "${white}[ENTER]${rst}"; read acept
		menu
	else
		echo "${red}El fichero $file_2_upload no existe${rst}";
		upload_anonfiles
	fi
fi
menu
}

upload_anonfiles_tor() {

	# Crear directorio temporal
if [ -d .temp_crypth/ ]; then
		rm -r .temp_crypth/
		mkdir .temp_crypth/
	else
		mkdir .temp_crypth/
fi

	echo ""

#Comprobar binarios requeridos cURL, tor y torify
if ! which curl > /dev/null; then
		echo "${red}El script no ha encontrado el binario de cURL en el \$PATH"
		echo "Porfavor, instala 'cURL' en el sistema${rst}"
		echo "${white}[ENTER]${rst}"; read acept
		menu
	elif ! which tor > /dev/null;
	then
		echo "${red}El script no ha encontrado el binario de TOR en el \$PATH"
		echo "Porfavor, instala 'tor' en el sistema${rst}"
		echo "${white}[ENTER]${rst}"; read acept
		menu
	elif ! which torify > /dev/null;
	then
		echo "${red}El script no ha encontrado el binario de torify en el \$PATH"
		echo "Porfavor, instala 'torify' en el sistema${rst}"
		echo "${white}[ENTER]${rst}"; read acept
		menu

	else

		echo "Se va a subir el fichero que indiques a ${blue}www.anonfiles.com${rst} a través de ${green}TOR${rst}, su fichero podría ser accesible por otras personas"
		echo -n "${purple} #${rst}"
		read -e -p ' Indique el fichero a enviar SIN ESPACIOS: ' file_2_upload
		echo ""
		echo "${green}Iniciando servicio de TOR...${rst}"
		if ! which systemctl > /dev/null; then
			echo ""
			echo "${red}Porfavor inicie el servicio de TOR manualmente, cuando lo haya iniciado, pulse ${rst}"
			echo "${white}[ENTER]${rst}"; read acept
			echo ""
			sleep 5
			echo "Subiendo fichero..."
			torify curl -# -F "file=@$file_2_upload" https://api.anonfiles.com/upload > .temp_crypth/.jsondata_anonfiles.api
			url_anonfiles="$(cat .temp_crypth/.jsondata_anonfiles.api | awk -F'"' '/full/{gsub(/\\/, "", $4); print $4}')"
			echo ""
			echo "Link de descarga: ${blue}$url_anonfiles${rst}"
			echo "${white}[ENTER]${rst}"; read acept
			menu
		else
		sudo systemctl start tor
		echo ""
		echo "Subiendo fichero..."
		sleep 5
		torify curl -# -F "file=@$file_2_upload" https://api.anonfiles.com/upload > .temp_crypth/.jsondata_anonfiles.api
		sudo systemctl stop tor
		url_anonfiles="$(cat .temp_crypth/.jsondata_anonfiles.api | awk -F'"' '/full/{gsub(/\\/, "", $4); print $4}')"
		echo ""
		echo "Link de descarga: ${blue}$url_anonfiles${rst}"
		echo "${white}[ENTER]${rst}"; read acept
		menu
	fi
fi
menu
}


#UTILIDADES y función para generar claves
gen_claves() {
	# Comprobar si el fichero existe o borrarlo si está vacío
	if [ -f ${USER}_rsa_private.key ];
        then
        echo " ${red}ATENCIÓN!${rst}"
		echo " El fichero "${green}${USER}_rsa_private.key${rst}" ya existe"
		echo "${white}[ENTER]${rst}"; read acept

		menu
		leer_opciones
        else
		# Info de que se va a generar
		echo ""
		echo "   Este script generará una clave Privada:"
		echo "   Algoritmo:	${green}RSA 4096-bits${rst}"
		echo "   Cifrado:	${green}Camellia256${rst}"
		echo "   Nombre:	${green}${USER}_rsa_private.key${rst}"
		echo "   Password:	${red}Será solicitada${rst}"
		echo ""
		echo "${white}[ENTER]${rst}"; read acept	
		
		# Generar clave Pública y Privada
		banner
		echo " ${red}SE LE SOLICITARÁ QUE INDIQUE CONTRASEÑA${rst} "
		openssl genpkey -algorithm RSA -out ${USER}_rsa_private.key -pkeyopt rsa_keygen_bits:4096 -camellia256
	fi

	# Comprobar si el fichero se ha generado vacío (por meter mal la contraseña y borrar el generado)
	# Si el fichero no está vacío, genera una clave pública de él
	if [ -s ${USER}_rsa_private.key ];
	then
		echo ""
		echo " Fichero ${green}${USER}_rsa_private.key${rst} generado correctamente :)"
		sleep 3
		echo ""
		echo " Generando clave pública PEM..."
		echo ""
		echo "Repita la contraseña"
		echo ""
		openssl rsa -in ${USER}_rsa_private.key -outform PEM -pubout -out ${USER}_rsa_public.pem
		if [ -s ${USER}_rsa_public.pem ];
		then
			echo ""
			echo " Fichero ${green}${USER}_rsa_public.pem${rst} generado correctamente :)"
			echo " Tus claves se han guardado en ${green}${PWD}${rst}"
			echo ""
			echo "${white}[ENTER]${rst}"; read acept

			menu
			leer_opciones
		else
			echo " ${red}Fichero generado con errores${rst}"
			echo " ${red}[ENTER]${rst}"
			echo "${white}[ENTER]${rst}"; read acept

			menu
			leer_opciones
		fi
	else
		echo ""
		echo " ${red}Fichero generado con errores${rst}"
		echo " ${red}[ENTER]${rst}"; read acept
		rm ${USER}_rsa_private.key
	fi

	menu
	leer_opciones
}

menu_estado_claves() {
        banner
        echo "    1. Comprobar salud clave ${green}Privada${rst}"
        echo "    2. Comprobar salud clave ${green}Pública${rst}"
        echo "    3. Volver al menú"
        echo ""

        local choice
        read -p "${white}Seleccione una opción [ 1 - 3 ] ${rst}" choice
        case $choice in
                1) estado_clave_privada ;;
                2) estado_clave_publica ;;
                3) menu leer_opciones ;;
                *) echo "${red}Escriba una opción válida${rst}" && sleep 2

        esac
}
# Comprobar estado de las claves
estado_clave_privada() {
	listado
    echo -n "${purple} #${rst}"
	read -e -p ' Indique la clave Privada a comprobar SIN ESPACIOS: ' privkey_to_check
	openssl rsa -check -in ${privkey_to_check} --noout
	echo "${white}[ENTER]${rst}"; read acept

	menu_estado_claves
}

estado_clave_publica() {
    listado
    echo -n "${purple} #${rst}"
	read -e -p ' Indique la clave Pública a comprobar SIN ESPACIOS: ' pubkey_to_check
	openssl pkey -inform PEM -pubin -in ${pubkey_to_check}
	echo "${white}[ENTER]${rst}"; read acept

	menu_estado_claves
}

actualizar_openssl() {
	echo "Este proceso sustituirá tu versión de OpenSSL a la ultima y va a tardar lo suyo..."
	cd /tmp
	sudo git clone https://github.com/openssl/openssl.git
	sudo mv openssl /usr/src
	cd /usr/src/openssl
	sudo ./config
	sudo make
	sudo make install
	sudo mv /usr/bin/openssl /root/
	sudo ln -s /usr/local/ssl/bin/openssl /usr/bin/openssl
	sudo ldconfig /usr/local/lib64/
	echo ""
	echo "Versión de OpenSSL instalada:"
	openssl version
	echo "${white}[ENTER]${rst}"; read acept
	menu
	leer_opciones
}

# Captura de señales

trap ksignal INT TERM

# COMIENZA LA EJECUCIÓN

# Confirmación
echo "${red}#################################################################################${rst}"
echo " 				   ${red}¡ATENCIÓN!${rst} "
echo ""
echo "   Este script debe ejecutarse con ${white}bash${rst}"
echo ""
echo "   Es muy importante que los usuarios que utilicen y cifren ficheros con este "
echo "   script tengan OpenSSL 3, actualiza tu versión de OpenSSL: "
echo "    ${green}bash ./CryptoHelper.sh --update-openssl${rst}"
echo ""
echo "   ¿No sabes lo que hace este fichero?"
echo "    ${green}bash ./CryptoHelper.sh -h|--help${rst} "
echo ""
echo "   Al continuar usted ha ${white}leído y comprende${rst} lo que hace este script "
echo "${red}#################################################################################${rst}"
echo ""
echo "${white}[ENTER]${rst}"; read acept

while true
do
	menu
	leer_opciones
done
